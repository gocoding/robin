//
//  State.swift
//  Robin
//
//  Created by 刘科 on 2023/8/21.
//

import Foundation

class State: Equatable {
    private var name: String
    
    required init() {
        let t = type(of: self)
        name = NSStringFromClass(t)
    }
    static func == (lhs: State, rhs: State) -> Bool {
        return lhs.name == rhs.name
    }
    
    func onEntry(stateMachine: StateMachine, from: State?) -> Void {
        
    }
    
    func onExit(stateMachine: StateMachine, to: State?) -> Void {
        
    }
    
    
}
