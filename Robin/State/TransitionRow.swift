//
//  TransitionRow.swift
//  Robin
//
//  Created by 刘科 on 2023/8/21.
//

import Foundation

class TransitionRow {
    private var stateMachine_: StateMachine
    private var begin_: State
    private var end_: State
    private var event_: String
    private var action_: ((TransitionRow) -> Bool)?
    
    init(stateMachine: StateMachine, begin: State, event: String, end: State, action: ((TransitionRow) -> Bool)?) {
        stateMachine_ = stateMachine
        begin_ = begin
        event_ = event
        end_ = end
        action_ = action
    }
    
    func process() -> Bool {
        guard let action = action_ else {return true}
        if action(self) {
            begin_.onExit(stateMachine: stateMachine_, to: end_)
            end_.onEntry(stateMachine: stateMachine_, from: begin_)
            return true
        }
        return false
    }
    
    func getEvent() -> String {
        return event_
    }
    
    func getBeginState() -> State {
        return begin_
    }
    
    func getEndState() -> State {
        return end_
    }
}
