//
//  StateMachine.swift
//  Robin
//
//  Created by 刘科 on 2023/8/21.
//

import Foundation

protocol StateMachineDelegate {
    func noTransition(stateMachine: StateMachine, event: String, from: State) -> Void;
}

class StateMachine {
    
    private var list: [TransitionRow] = []
    private var stateObjDic: [String: State] = [:]
    private var current: State
    private var initState: State?
    
    var delegate: StateMachineDelegate?
    
    init() {
        current = State()
    }
    
    func setInitState(state: State) {
        initState = state
    }
    
    func addRow(begin: State.Type, event: String, end: State.Type, action: ((TransitionRow) -> Bool)?) {
        let beginString = NSStringFromClass(begin)
        var beginObj = stateObjDic[beginString]
        if beginObj == nil {
            beginObj = begin.init()
            stateObjDic[beginString] = beginObj
        }
        
        let endString = NSStringFromClass(end)
        var endObj = stateObjDic[endString]
        if endObj == nil {
            endObj = end.init()
            stateObjDic[endString] = endObj
        }
        if let beginObj = beginObj, let endObj = endObj {
            addRow(row: TransitionRow(stateMachine: self, begin: beginObj, event: event, end: endObj, action: action))
        }
    }
    
    private func addRow(row: TransitionRow) -> Void {
        list.append(row)
    }
    
    func start() {
        if let initState = initState {
            current = initState
            current.onEntry(stateMachine: self, from: nil)
        }
    }
    
    func processEvent(event: String) -> Void {
        if let row = findRow(event: event) {
            if row.process() {
                current.onExit(stateMachine: self, to: row.getEndState())
                current = row.getEndState()
                current.onEntry(stateMachine: self, from: row.getBeginState())
            }
        } else {
            if let delegate = self.delegate {
                delegate.noTransition(stateMachine: self, event: event, from: current)
            }
        }
    }
    
    private func findRow(event: String) -> TransitionRow? {
        for row in list {
            if row.getEvent() == event && row.getBeginState() == current {
                //find
                return row
            }
        }
        return nil
    }
}
