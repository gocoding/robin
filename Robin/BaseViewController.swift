//
//  BaseViewController.swift
//  Robin
//
//  Created by 刘科 on 2023/8/10.
//

import AppKit

class BaseViewController: NSViewController {
    override func loadView() {
        self.view = NSView()
    }
    
    override var nibName: NSNib.Name? {
        return self.className
    }
}
