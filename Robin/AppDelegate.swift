//
//  AppDelegate.swift
//  Robin
//
//  Created by 刘科 on 2023/8/2.
//

import Cocoa

@main
class AppDelegate: NSResponder, NSApplicationDelegate {

    var window: NSWindow?
    
    static func main() {
        let app = NSApplication.shared //创建应用
        let delegate = AppDelegate()
        app.delegate =  delegate //配置应用代理
        app.mainMenu =  nil// mainMenu() //配置菜单，mainMenu 函数需要前向定义，否则编译错误
        app.run() //启动应用
    }
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        let frame = CGRect(x: 0, y: 0, width: 1600, height: 800)
        window = NSWindow(contentRect: frame, styleMask: [.titled, .closable, .resizable], backing: .buffered, defer: false)
//        window?.styleMask = [.unifiedTitleAndToolbar, .titled]
//        window?.setFrame(frame, display: true)
        window?.center()
        window?.title = "Robin Tests"
//        window?.backgroundColor = NSColor.clear
//        window?.alphaValue = 0.5
        
        let mainViewController = MainViewController()
        mainViewController.view.frame = frame
        window?.contentViewController = mainViewController
        
        window?.makeKeyAndOrderFront(nil)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func applicationSupportsSecureRestorableState(_ app: NSApplication) -> Bool {
        return true
    }

    func application(_ application: NSApplication, open urls: [URL]) {
        
    }

    func applicationDidBecomeActive(_ notification: Notification) {
        
    }
    
}

//@main
//func Main() -> Void {
//    print("")
//}
//@main
//class Main {
//    init() {
//        autoreleasepool {
//            let app =   NSApplication.shared //创建应用
//            let delegate = AppDelegate()
//            app.delegate =  delegate //配置应用代理
//            app.mainMenu =  nil// mainMenu() //配置菜单，mainMenu 函数需要前向定义，否则编译错误
//            app.run() //启动应用
//        }
//    }
//
//    static func main() {
//
//    }
//}

