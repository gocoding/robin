//
//  RenderShaderTypes.h
//  Robin
//
//  Created by Liuke on 2023/7/20.
//

#ifndef RenderShaderTypes_h
#define RenderShaderTypes_h

#include <simd/simd.h>

typedef struct {
    vector_float4 position;
    vector_float2 texture_coordinate;
} RenderVertex;

typedef struct {
    matrix_float3x3 matrix;
    vector_float3 offset;
} RenderConvertMatrix;


#endif /* RenderShaderTypes_h */
