//
//  RenderView.swift
//  Robin
//
//  Created by Liuke on 2023/7/20.
//
//#if TARGET_OS_IPHONE
//
//#elseif TARGET_OS_MAC
//
//#endif

#if os(OSX)
import AppKit
typealias BaseViewType = NSView
#elseif os(iOS)
import UIKit
typealias BaseViewType = UIView
#endif

import MetalKit


class RenderView: BaseViewType, MTKViewDelegate {

    private lazy var mtkView: MTKView = {
        let view = MTKView(frame: self.bounds)
        view.device = MTLCreateSystemDefaultDevice()
        view.delegate = self
        addSubview(view)
        return view
    }()

    private lazy var commandQueue: MTLCommandQueue? = {
        return self.mtkView.device?.makeCommandQueue()
    }()

    private var vertices: [RenderVertex] = []

    private var texture: MTLTexture?
    private var yTexture: MTLTexture?
    private var uvTexture: MTLTexture?
    private var size: CGSize = CGSizeZero
    private var textureSize: CGSize = CGSizeZero
    private var pipelineState: MTLRenderPipelineState?
    private var yuvPipelineState: MTLRenderPipelineState?
    private var verticesBuffer: MTLBuffer?
    private var matrixBuffer: MTLBuffer?
    private lazy var textureCache: CVMetalTextureCache? = {
        var value: CVMetalTextureCache?
        if let device = self.mtkView.device, CVMetalTextureCacheCreate(nil, nil, device, nil, &value) == kCVReturnSuccess, let value = value {
            return value
        }
        return nil
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        size = frame.size
        mtkView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: frame.size)
        updateVertices()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

#if os(OSX)
    override func resizeSubviews(withOldSize oldSize: NSSize) {
        mtkView.frame = bounds
        if (!size.equalTo(bounds.size)) {
            size = bounds.size
            if mode == .Fit || mode == .AspectFill {
                updateVertices()
            }
        }
    }
    override func layoutSubtreeIfNeeded() {
        mtkView.frame = bounds
    }
#elseif os(iOS)
    override func layoutSubviews() {
        mtkView.frame = bounds
        if (!size.equalTo(bounds.size)) {
            size = bounds.size
            if mode == .Fit || mode == .AspectFill {
                updateVertices()
            }
        }
    }

#endif


    func setupRenderPipeline() -> Void {
        let lib = mtkView.device?.makeDefaultLibrary()
        let pipeline = MTLRenderPipelineDescriptor()
        pipeline.vertexFunction = lib?.makeFunction(name: "vertexShader")
        pipeline.fragmentFunction = lib?.makeFunction(name: "fragmentShader")
        pipeline.colorAttachments[0].pixelFormat = mtkView.colorPixelFormat
        pipelineState = try? mtkView.device?.makeRenderPipelineState(descriptor: pipeline)
    }

    func setupYUVRenderPipeline() -> Void {
        let lib = mtkView.device?.makeDefaultLibrary()
        let pipeline = MTLRenderPipelineDescriptor()
        pipeline.vertexFunction = lib?.makeFunction(name: "vertexShader")
        pipeline.fragmentFunction = lib?.makeFunction(name: "fragmentYUVShader")
        pipeline.colorAttachments[0].pixelFormat = mtkView.colorPixelFormat
        yuvPipelineState = try? mtkView.device?.makeRenderPipelineState(descriptor: pipeline)

        let matrix = matrix_float3x3(columns: (simd_float3(1, 1, 1), simd_float3(0, -0.343, 1.765), simd_float3(1.4, -0.711, 0)))
        let offset = vector_float3(-(16.0/255.0), -0.5, -0.5)
        var m = RenderConvertMatrix(matrix: matrix, offset: offset)
        self.matrixBuffer = mtkView.device?.makeBuffer(bytes: &m, length: MemoryLayout<RenderConvertMatrix>.size, options: .storageModeShared)
    }

#if os(OSX)
    func update(image: NSImage) {
        yTexture = nil
        uvTexture = nil
        if pipelineState == nil {
            setupRenderPipeline()
        }
        let preTextureSize = textureSize
        textureSize = CGSize(width: Int(image.size.width), height: Int(image.size.height))
        if (!textureSize.equalTo(preTextureSize)) {
            updateVertices()
        }
        let textureDesc = MTLTextureDescriptor()
        textureDesc.pixelFormat = .rgba8Unorm
        textureDesc.width = Int(textureSize.width)
        textureDesc.height = Int(textureSize.height)
        texture = mtkView.device?.makeTexture(descriptor: textureDesc)
        if let date = getBytes(image: image) {
            updateTexture(bytes: date, width: Int(textureSize.width), height: Int(textureSize.height))
            date.deallocate()
        }
    }
#elseif os(iOS)
    func update(image: UIImage) {
        yTexture = nil
        uvTexture = nil
        if pipelineState == nil {
            setupRenderPipeline()
        }
        let preTextureSize = textureSize
        textureSize = CGSize(width: Int(image.size.width * image.scale), height: Int(image.size.height * image.scale))
        if (!textureSize.equalTo(preTextureSize)) {
            updateVertices()
        }
        let textureDesc = MTLTextureDescriptor()
        textureDesc.pixelFormat = .rgba8Unorm
        textureDesc.width = Int(textureSize.width)
        textureDesc.height = Int(textureSize.height)
        texture = mtkView.device?.makeTexture(descriptor: textureDesc)
        if let date = getBytes(image: image) {
            updateTexture(bytes: date, width: Int(textureSize.width), height: Int(textureSize.height))
            date.deallocate()
        }
    }
#endif
    


    func update(pixelBuffer: CVPixelBuffer) {
        texture = nil;
        if yuvPipelineState == nil {
            setupYUVRenderPipeline()
        }

        CVPixelBufferLockBaseAddress(pixelBuffer, [])
        defer {CVPixelBufferUnlockBaseAddress(pixelBuffer, [])}

        do {
            let w = CVPixelBufferGetWidthOfPlane(pixelBuffer, 0)
            let h = CVPixelBufferGetHeightOfPlane(pixelBuffer, 0)
            let bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 0)

            if (Int(textureSize.width) != w || Int(textureSize.height) != h) {
                textureSize = CGSize(width: w, height: h)
                updateVertices()
            }

            let yTextureDesc = MTLTextureDescriptor()
            yTextureDesc.pixelFormat = .r8Unorm
            yTextureDesc.width = Int(w)
            yTextureDesc.height = Int(h)
            yTexture = mtkView.device?.makeTexture(descriptor: yTextureDesc)

            if let addr = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0) {
                let yregion = MTLRegion(origin: MTLOrigin(x: 0, y: 0, z: 0), size: MTLSize(width: w, height: h, depth: 1))
                yTexture?.replace(region: yregion, mipmapLevel: 0, withBytes: addr, bytesPerRow: bytesPerRow)
            }

        }

        do {
            let w = CVPixelBufferGetWidthOfPlane(pixelBuffer, 1)
            let h = CVPixelBufferGetHeightOfPlane(pixelBuffer, 1)
            let bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 1)

            let uvTextureDesc = MTLTextureDescriptor()
            uvTextureDesc.pixelFormat = .rg8Unorm
            uvTextureDesc.width = Int(w)
            uvTextureDesc.height = Int(h)
            uvTexture = mtkView.device?.makeTexture(descriptor: uvTextureDesc)

            if let addr = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 1) {
                let uvregion = MTLRegion(origin: MTLOrigin(x: 0, y: 0, z: 0), size: MTLSize(width: w, height: h, depth: 1))
                uvTexture?.replace(region: uvregion, mipmapLevel: 0, withBytes: addr, bytesPerRow: bytesPerRow)
            }
        }
    }

    var scale: Float = 1 {
        didSet {
            updateVertices()
        }
    }
    enum RenderMode {
        case Fill //填充，比例会变化
        case Fit //等比缩放，会留下空白
        case AspectFill //等比缩放，填充整个视图
    }
    var mode: RenderMode = .Fit {
        didSet {
            updateVertices()
        }
    }

    private func updateVertices() {
        switch mode {
        case .Fill:
            self.vertices = createVertices4Fill()
        case .Fit:
            self.vertices = createVertices4Fit(textureWidth: Int(textureSize.width), textureHeight: Int(textureSize.height),
                                               viewWidth: Int(size.width), ViewHeight: Int(size.height))
        case .AspectFill:
            self.vertices = createVertices4AspectFill(textureWidth: Int(textureSize.width), textureHeight: Int(textureSize.height),
                                                      viewWidth: Int(size.width), ViewHeight: Int(size.height))
        }
        if (!vertices.isEmpty) {
            verticesBuffer = mtkView.device?.makeBuffer(bytes: vertices, length: MemoryLayout<RenderVertex>.size * vertices.count, options: .storageModeShared)
        }
    }

    private func updateTexture(bytes: UnsafeRawPointer, width: Int, height: Int) {
        let region = MTLRegion(origin: MTLOrigin(x: 0, y: 0, z: 0), size: MTLSize(width: width, height: height, depth: 1))
        texture?.replace(region: region, mipmapLevel: 0, withBytes: bytes, bytesPerRow: width * 4)
    }

#if os(OSX)
    private func getBytes(image: NSImage) -> UnsafePointer<UInt8>? {
        if let data = image.tiffRepresentation {
            let bitmap = NSBitmapImageRep .imageReps(with: data)
//            let s = bitmap.first.
            let w: Int = Int(textureSize.width);
            let h: Int = Int(textureSize.height);
            let size = Int(w * h) * MemoryLayout<UInt8>.size * 4
            let pointer = UnsafeMutablePointer<UInt8>.allocate(capacity: size)
            data.copyBytes(to: pointer, count: size)
            return UnsafePointer<UInt8>(pointer)
        }
        
        return nil
    }
#elseif os(iOS)
    private func getBytes(image: UIImage) -> UnsafePointer<UInt8>? {
        guard let cgImage = image.cgImage else {return nil}

        let w: Int = Int(textureSize.width);
        let h: Int = Int(textureSize.height);

        let pointer = UnsafeMutablePointer<UInt8>.allocate(capacity: Int(w * h) * MemoryLayout<UInt8>.size * 4)
        if let space = cgImage.colorSpace, let context = CGContext(data: pointer, width: w, height: h, bitsPerComponent: 8, bytesPerRow: w * 4, space: space, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) {
            context.draw(cgImage, in: CGRect(x: 0, y: 0, width: w, height: h))
            return UnsafePointer<UInt8>(pointer)
        }
        return nil
    }
#endif

    private func createVertices4Fill() -> [RenderVertex] {
        var array: [RenderVertex] = []
        let v1 = RenderVertex(position: vector_float4(scale, -scale, 0.0, 1.0), texture_coordinate: vector_float2(1, 1))
        array.append(v1)
        let v2 = RenderVertex(position: vector_float4(-scale, -scale, 0.0, 1.0), texture_coordinate: vector_float2(0, 1))
        array.append(v2)
        let v3 = RenderVertex(position: vector_float4(-scale, scale, 0.0, 1.0), texture_coordinate: vector_float2(0, 0))
        array.append(v3)

        let v4 = RenderVertex(position: vector_float4(scale, -scale, 0.0, 1.0), texture_coordinate: vector_float2(1, 1))
        array.append(v4)
        let v5 = RenderVertex(position: vector_float4(-scale, scale, 0.0, 1.0), texture_coordinate: vector_float2(0, 0))
        array.append(v5)
        let v6 = RenderVertex(position: vector_float4(scale, scale, 0.0, 1.0), texture_coordinate: vector_float2(1, 0))
        array.append(v6)
        return array
    }

    private func createVertices4Fit(textureWidth: Int, textureHeight: Int, viewWidth: Int, ViewHeight: Int) -> [RenderVertex] {
        if (textureWidth > 0 && textureHeight > 0 && viewWidth > 0 && ViewHeight > 0) {
            let widthRate = Float(textureWidth) / Float(viewWidth)
            let heightRate = Float(textureHeight) / Float(ViewHeight)

            var lt_x: Float = 0
            var lt_y: Float = 0
            var lb_x: Float = 0
            var lb_y: Float = 0
            var rt_x: Float = 0
            var rt_y: Float = 0
            var rb_x: Float = 0
            var rb_y: Float = 0
            if widthRate > heightRate {
                //以宽度对齐
                let rate = (Float(textureHeight) / Float(textureWidth)) / (Float(ViewHeight) / Float(viewWidth))
                lt_x = -1 * scale
                lt_y = rate * scale

                lb_x = -1 * scale
                lb_y = -rate * scale

                rt_x = 1 * scale
                rt_y = rate * scale

                rb_x = 1 * scale
                rb_y = -rate * scale
            } else {
                //高度对齐
                let rate = (Float(textureWidth) / Float(textureHeight)) / (Float(viewWidth) / Float(ViewHeight))
                lt_x = -rate * scale
                lt_y = 1 * scale

                lb_x = -rate * scale
                lb_y = -1 * scale

                rt_x = rate * scale
                rt_y = 1 * scale

                rb_x = rate * scale
                rb_y = -1 * scale
            }

            var array: [RenderVertex] = []
            let v1 = RenderVertex(position: vector_float4(rb_x, rb_y, 0.0, 1.0), texture_coordinate: vector_float2(1, 1))
            array.append(v1)
            let v2 = RenderVertex(position: vector_float4(lb_x, lb_y, 0.0, 1.0), texture_coordinate: vector_float2(0, 1))
            array.append(v2)
            let v3 = RenderVertex(position: vector_float4(lt_x, lt_y, 0.0, 1.0), texture_coordinate: vector_float2(0, 0))
            array.append(v3)

            let v4 = RenderVertex(position: vector_float4(rb_x, rb_y, 0.0, 1.0), texture_coordinate: vector_float2(1, 1))
            array.append(v4)
            let v5 = RenderVertex(position: vector_float4(lt_x, lt_y, 0.0, 1.0), texture_coordinate: vector_float2(0, 0))
            array.append(v5)
            let v6 = RenderVertex(position: vector_float4(rt_x, rt_y, 0.0, 1.0), texture_coordinate: vector_float2(1, 0))
            array.append(v6)
            return array
        } else {
            return []
        }
    }

    private func createVertices4AspectFill(textureWidth: Int, textureHeight: Int, viewWidth: Int, ViewHeight: Int) -> [RenderVertex] {
        if (textureWidth > 0 && textureHeight > 0 && viewWidth > 0 && ViewHeight > 0) {
            let widthRate = Float(textureWidth) / Float(viewWidth)
            let heightRate = Float(textureHeight) / Float(ViewHeight)

            var lt_x: Float = 0
            var lt_y: Float = 0
            var lb_x: Float = 0
            var lb_y: Float = 0
            var rt_x: Float = 0
            var rt_y: Float = 0
            var rb_x: Float = 0
            var rb_y: Float = 0
            if widthRate < heightRate {
                //以宽度对齐
                let rate = (Float(textureHeight) / Float(textureWidth)) / (Float(ViewHeight) / Float(viewWidth))
                lt_x = -1 * scale
                lt_y = rate * scale

                lb_x = -1 * scale
                lb_y = -rate * scale

                rt_x = 1 * scale
                rt_y = rate * scale

                rb_x = 1 * scale
                rb_y = -rate * scale
            } else {
                //高度对齐
                let rate = (Float(textureWidth) / Float(textureHeight)) / (Float(viewWidth) / Float(ViewHeight))
                lt_x = -rate * scale
                lt_y = 1 * scale

                lb_x = -rate * scale
                lb_y = -1 * scale

                rt_x = rate * scale
                rt_y = 1 * scale

                rb_x = rate * scale
                rb_y = -1 * scale
            }

            var array: [RenderVertex] = []
            let v1 = RenderVertex(position: vector_float4(rb_x, rb_y, 0.0, 1.0), texture_coordinate: vector_float2(1, 1))
            array.append(v1)
            let v2 = RenderVertex(position: vector_float4(lb_x, lb_y, 0.0, 1.0), texture_coordinate: vector_float2(0, 1))
            array.append(v2)
            let v3 = RenderVertex(position: vector_float4(lt_x, lt_y, 0.0, 1.0), texture_coordinate: vector_float2(0, 0))
            array.append(v3)

            let v4 = RenderVertex(position: vector_float4(rb_x, rb_y, 0.0, 1.0), texture_coordinate: vector_float2(1, 1))
            array.append(v4)
            let v5 = RenderVertex(position: vector_float4(lt_x, lt_y, 0.0, 1.0), texture_coordinate: vector_float2(0, 0))
            array.append(v5)
            let v6 = RenderVertex(position: vector_float4(rt_x, rt_y, 0.0, 1.0), texture_coordinate: vector_float2(1, 0))
            array.append(v6)
            return array
        } else {
            return []
        }
    }

    //MARK: mtkview delegate
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        self.size = size
    }
    func draw(in view: MTKView) {
        if !vertices.isEmpty,
            let buf = commandQueue?.makeCommandBuffer(),
            let renderPass = mtkView.currentRenderPassDescriptor,
            let encoder = buf.makeRenderCommandEncoder(descriptor: renderPass) {
#if os(OSX)
                let windowScale: CGFloat = 2
#elseif os(iOS)
                let windowScale: CGFloat = UIScreen.main.scale
#endif
                if yTexture != nil && uvTexture != nil {
                    if let pipelineState = self.yuvPipelineState {
                        renderPass.colorAttachments[0].clearColor = MTLClearColor(red: 0, green: 0.5, blue: 0.5, alpha: 1)
                        encoder.setViewport(MTLViewport(originX: 0, originY: 0, width: size.width * windowScale, height: size.height * windowScale, znear: 0, zfar: 1))
                        encoder.setRenderPipelineState(pipelineState)
                        encoder.setVertexBuffer(verticesBuffer, offset: 0, index: 0)
                        encoder.setFragmentTexture(yTexture, index: 0)
                        encoder.setFragmentTexture(uvTexture, index: 1)
                        encoder.setFragmentBuffer(self.matrixBuffer, offset: 0, index: 0)
                        encoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: vertices.count)
                        encoder.endEncoding()
                        if let currentDrawable = mtkView.currentDrawable {
                            buf.present(currentDrawable)
                        }
                        buf.commit()
                    }
                } else {
                    if let pipelineState = self.pipelineState {
                        renderPass.colorAttachments[0].clearColor = MTLClearColor(red: 0, green: 0.5, blue: 0.5, alpha: 1)
                        encoder.setViewport(MTLViewport(originX: 0, originY: 0, width: size.width * windowScale, height: size.height * windowScale, znear: 0, zfar: 1))
                        encoder.setRenderPipelineState(pipelineState)
                        encoder.setVertexBuffer(verticesBuffer, offset: 0, index: 0)
                        encoder.setFragmentTexture(texture, index: 0)
                        encoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: vertices.count)
                        encoder.endEncoding()
                        if let currentDrawable = mtkView.currentDrawable {
                            buf.present(currentDrawable)
                        }
                        buf.commit()
                    }
                }

        }
    }
}
