//
//  render.metal
//  Robin
//
//  Created by Liuke on 2023/7/20.
//

#include <metal_stdlib>
#include "RenderShaderTypes.h"

using namespace metal;

typedef struct {
    float4 position [[position]]; // position的修饰符表示这个是顶点
    
    float2 texture_coordinate; // 纹理坐标，会做插值处理
    
} RenderData;

vertex RenderData vertexShader(uint vertexID [[ vertex_id ]], // vertex_id是顶点shader每次处理的index，用于定位当前的顶点
             constant RenderVertex *vertexArray [[ buffer(0) ]]) { // buffer表明是缓存数据，0是索引
    RenderData out;
    out.position = vertexArray[vertexID].position;
    out.texture_coordinate = vertexArray[vertexID].texture_coordinate;
    return out;
}

fragment float4 fragmentShader(RenderData input [[stage_in]], // stage_in表示这个数据来自光栅化。（光栅化是顶点处理之后的步骤，业务层无法修改）
                               texture2d<half> colorTexture [[ texture(0) ]]) {// texture表明是纹理数据，0是索引
    constexpr sampler textureSampler (mag_filter::linear,
                                      min_filter::linear); // sampler是采样器

    half4 colorSample = colorTexture.sample(textureSampler, input.texture_coordinate); // 得到纹理对应位置的颜色

    return float4(colorSample);
}

fragment float4 fragmentYUVShader(RenderData input [[stage_in]],
                                  texture2d<float> yTexture [[texture(0)]],
                                  texture2d<float> uvTexture [[texture(1)]],
                                  constant RenderConvertMatrix *matrix [[buffer(0)]]
                                  ) {
    constexpr sampler textureSampler (mag_filter::linear, min_filter::linear);
    float y = yTexture.sample(textureSampler, input.texture_coordinate).r;
    float2 uv = uvTexture.sample(textureSampler, input.texture_coordinate).rg;

    float3 yuv = float3(y, uv);
    float3 rgb = matrix->matrix * (yuv + matrix->offset);
    return float4(rgb, 1.0);
//    return float4(1, 0, 0, 1.0);
}
