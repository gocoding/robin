//
//  MainViewController.swift
//  Robin
//
//  Created by 刘科 on 2023/8/2.
//

import AppKit
import SnapKit

class MainViewController: BaseViewController {
    
    private var renderView: RenderView?
    private var player: MoviePlayer?
    private var t: BaseThread?
    private var count: Int = 0
    private var wait = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.wantsLayer = true
        self.view.layer?.backgroundColor = NSColor.gray.cgColor
        
//        let renderView = RenderView(frame: self.view.bounds)
//        view.addSubview(renderView)
//        renderView.snp.makeConstraints { make in
//            make.edges.equalTo(view)
//        }
//        renderView.mode = .Fit
//        renderView.scale = 1
//
//        self.renderView = renderView
        
//        display(index: 1)
//        displayYUV()
        
//        player = MoviePlayer()
//        if let renderView = player?.getPlayerView() {
//            view.addSubview(renderView)
//            renderView.snp.makeConstraints { make in
//                make.edges.equalTo(view)
//            }
//            player?.set(url: "/Users/liuke/Downloads/test.mp4")
//            player?.play()
//        }
        
        
        
//        let mp4 = Mp4Player()
//        DispatchQueue.global().async {
//            mp4.read { pixelBuffer in
//                DispatchQueue.main.async {
//                    renderView.update(pixelBuffer: pixelBuffer)
//                }
//            }
//        }
        
        self.t = BaseThread(name: "test", action: {
            if self.count > 10000000 {
                print("run\n")
                self.count = 0
            } else {
                self.count += 1
            }
        })
        self.t?.start()
        
        let btn = NSButton()
        btn.wantsLayer = true;
        btn.layer?.backgroundColor = NSColor.red.cgColor;
        view.addSubview(btn)
        btn.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(100)
            make.top.equalToSuperview().offset(100)
            make.width.equalTo(100)
            make.height.equalTo(100)
        }
        btn.target = self
        btn.action = #selector(click)
    }
    
    @objc func click() {
//        self.t?.setExit()
//        self.t = nil
        self.t?.setWait(wait)
        wait = !wait
    }
    
    func display(index: Int) -> Void {
        let path = String(format: "/Users/liuke/Downloads/image_group/foo-%05d.jpeg", index)
        let url = URL(filePath: path)
        if let data = try? Data(contentsOf: url) {
            if let img = NSImage(data: data) {
                renderView?.update(image: img)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.033, execute: {
                self.display(index: index + 1)
            })
        }
        
        
    }
    
    func displayYUV() -> Void {
        let path = "/Users/liuke/Downloads/01_I420_3500x2000.yuv"
        if FileManager.default.fileExists(atPath: path) {
            let url = URL(filePath: path)
            if let data = try? Data(contentsOf: url) {
                if let pixel = YUVConvertor.createCVPixelBufferRef(fromBuffer: data, type: .I420, width: 3500, height: 2000).takeUnretainedValue() as? CVPixelBuffer {
                    renderView?.update(pixelBuffer: pixel)
                }
            }
        }
    }
}
