//
//  IPlayer.swift
//  Robin
//
//  Created by 刘科 on 2023/8/20.
//

import Foundation

protocol IPlayerControl {
    func onPlayer(current: TimeInterval, total: TimeInterval)
}

protocol IPlayer {
    func getPlayerView() -> NSView
    func set(url: String)
    func play();
    func pause();
    func stop();
    var delegate: IPlayerControl? {set get}
}
