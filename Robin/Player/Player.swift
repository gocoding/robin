//
//  Player.swift
//  Robin
//
//  Created by 刘科 on 2023/8/20.
//

import Foundation

class Player {
    private var player: IPlayer?
    
    init(file: String) {
        player = MoviePlayer()
        player?.set(url: file)
    }
    
    func play() {
        player?.play()
    }
    
    func pause() {
        player?.pause()
    }
    
    func stop() {
        player?.stop()
    }
    
    
}
