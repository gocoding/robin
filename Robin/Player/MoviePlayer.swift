//
//  MoviePlayer.swift
//  Robin
//
//  Created by 刘科 on 2023/8/20.
//

import Foundation


class MoviePlayer: IPlayer, StateMachineDelegate {
    
    private var fsm: MoviePlayerStateMachine
    
    private class MoviePlayerStateMachine: StateMachine {
        var player: MoviePlayer?
    }
    
    private class MoviePlayerInitState: State {
        override func onExit(stateMachine: StateMachine, to: State?) {
            print("on exit MoviePlayerInitState")
            
        }
        
        override func onEntry(stateMachine: StateMachine, from: State?) {
            print("on entry MoviePlayerInitState")
            if let fsm = stateMachine as? MoviePlayerStateMachine {
                //初始化播放器
                fsm.player?.player = Mp4Player()
                fsm.player?.player?.initContext()
            }
        }
    }

    private class MoviePlayerPreparedState: State {
        override func onExit(stateMachine: StateMachine, to: State?) {
            print("on exit MoviePlayerPreparedState")
        }
        
        override func onEntry(stateMachine: StateMachine, from: State?) {
            print("on entry MoviePlayerPreparedState")
            if let fsm = stateMachine as? MoviePlayerStateMachine, let url = fsm.player?.url {
                fsm.player?.player?.openUrl(url)
            }
        }
    }

    private class MoviePlayerPlayingState: State {
        override func onExit(stateMachine: StateMachine, to: State?) {
            print("on exit MoviePlayerPlayingState")
        }
        
        override func onEntry(stateMachine: StateMachine, from: State?) {
            print("on entry MoviePlayerPlayingState")
            if let fsm = stateMachine as? MoviePlayerStateMachine {
                fsm.player?.videoQueue.async {
                    while true {
                        if let px = fsm.player?.player?.readVideoFrame() {
                            DispatchQueue.main.async {
                                fsm.player?.render(pixel: px.takeRetainedValue())
                            }
                            Thread.sleep(forTimeInterval: 0.03)
                        }
                    }
                }
            }
        }
    }

    private class MoviePlayerPausedState: State {
        override func onExit(stateMachine: StateMachine, to: State?) {
            print("on exit MoviePlayerPausedState")
        }
        
        override func onEntry(stateMachine: StateMachine, from: State?) {
            print("on entry MoviePlayerPausedState")
        }
    }

    private class MoviePlayerStopedState: State {
        override func onExit(stateMachine: StateMachine, to: State?) {
            print("on exit MoviePlayerStopedState")
        }
        
        override func onEntry(stateMachine: StateMachine, from: State?) {
            print("on entry MoviePlayerStopedState")
        }
    }
    
    private class MoviePlayerCompletedState: State {
        override func onExit(stateMachine: StateMachine, to: State?) {
            print("on exit MoviePlayerCompletedState")
            
        }
        
        override func onEntry(stateMachine: StateMachine, from: State?) {
            print("on entry MoviePlayerCompletedState")
        }
    }
    
    private static let PREPARE = "prepare" //init -> prepare
    private static let PLAY = "play" //prepare -> playing
    private static let PAUSE = "pause" //playing -> paused
    private static let STOP = "stop" //playing -> stop, paused -> stop, prepare -> stop
    private static let COMPLETE = "complete" //playing -> completed
    
    private var player: Mp4Player?
    private var url: String?
    private var videoQueue: DispatchQueue
    private var renderView: RenderView
    
    init() {
        fsm = MoviePlayerStateMachine()
        videoQueue = DispatchQueue(label: "com.video.queue")
        renderView = RenderView()
        
        fsm.player = self
        
        fsm.addRow(begin: MoviePlayerInitState.self, event: MoviePlayer.PREPARE, end: MoviePlayerPreparedState.self, action: { row in
            if let url = self.url, !url.isEmpty {
                return true
            }
            return false
        })
        fsm.addRow(begin: MoviePlayerPreparedState.self, event: MoviePlayer.PLAY, end: MoviePlayerPlayingState.self, action: nil)
        fsm.addRow(begin: MoviePlayerPlayingState.self, event: MoviePlayer.PAUSE, end: MoviePlayerPausedState.self, action: nil)
        fsm.addRow(begin: MoviePlayerPlayingState.self, event: MoviePlayer.STOP, end: MoviePlayerStopedState.self, action: nil)
        fsm.addRow(begin: MoviePlayerPausedState.self, event: MoviePlayer.STOP, end: MoviePlayerStopedState.self, action: nil)
        fsm.addRow(begin: MoviePlayerPreparedState.self, event: MoviePlayer.STOP, end: MoviePlayerStopedState.self, action: nil)
        fsm.addRow(begin: MoviePlayerPlayingState.self, event: MoviePlayer.COMPLETE, end: MoviePlayerCompletedState.self, action: nil)
        
        fsm.setInitState(state: MoviePlayerInitState())
        
        fsm.start()
    }
    
    func getPlayerView() -> NSView {
        return renderView
    }
    
    func set(url: String) {
        self.url = url
        fsm.processEvent(event: MoviePlayer.PREPARE)
    }
    
    func play() {
        fsm.processEvent(event: MoviePlayer.PLAY)
    }
    
    func pause() {
        fsm.processEvent(event: MoviePlayer.PAUSE)
    }
    
    func stop() {
        fsm.processEvent(event: MoviePlayer.STOP)
    }
    
    var delegate: IPlayerControl?
    
    
    func noTransition(stateMachine: StateMachine, event: String, from: State) {
        
    }
    
    private func render(pixel: CVPixelBuffer) {
        renderView.update(pixelBuffer: pixel)
    }
}
