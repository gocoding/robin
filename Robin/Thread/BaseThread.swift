//
//  AVThread.swift
//  Robin
//
//  Created by 刘科 on 2023/8/30.
//

import Foundation

class BaseThread {
    private var queue_: DispatchQueue
    private var action_: () -> Void
    private var isExit = false
    private var isWait = false
    private var semaphore = DispatchSemaphore(value: 1)
    private var lock: NSLock = NSLock()
    
    init(name: String, action: @escaping ()->Void) {
        queue_ = DispatchQueue(label: name)
        action_ = action
    }
    
    func setExit() {
        lock.lock()
        isExit = true
        lock.unlock()
    }
    
    func setWait(_ wait: Bool) {
        lock.lock()
        if wait {
            self.isWait = true
        } else if self.isWait {
            self.isWait = false
            self.semaphore.signal()
        }
        lock.unlock()
    }
    
    func start() {
        queue_.async {
            while true {
                self.lock.lock()
                let exit = self.isExit
                let wait = self.isWait
                self.lock.unlock()
                
                if exit {
                    break
                }
                if wait {
                    self.semaphore.wait()
                }
                
                self.action_()
            }
            
        }
    }
    
}
