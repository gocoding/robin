//
//  Mp4Reader.h
//  Pods
//
//  Created by 刘科 on 2023/8/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Mp4Player : NSObject
@property (nonatomic, strong) NSString *file;
- (void)initContext;
- (void)openUrl:(NSString *)url;
- (void)read;
- (nullable CVPixelBufferRef)readVideoFrame;
//- (void)read:(void(^)(CVPixelBufferRef))callback;
@end

NS_ASSUME_NONNULL_END
