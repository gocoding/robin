//
//  Mp4Reader.m
//  Pods
//
//  Created by 刘科 on 2023/8/12.
//

#import "Mp4Player.h"
#import "YUVConvertor.h"
extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
}

@interface Mp4Player()
@end

@implementation Mp4Player {
    AVFormatContext *fmt_ctx;
    AVCodecContext *avctx;
    AVPacket *packet;
    AVFrame *frame;
}

- (void)initContext {
    fmt_ctx = avformat_alloc_context();
}

- (void)openUrl:(NSString *)url {
    int err = avformat_open_input(&fmt_ctx, [url UTF8String], NULL, NULL);
    
    avctx = avcodec_alloc_context3(NULL);
    int ret = avcodec_parameters_to_context(avctx, fmt_ctx->streams[0]->codecpar);
    const AVCodec *codec = avcodec_find_decoder(avctx->codec_id);
    ret = avcodec_open2(avctx, codec, NULL);
    
    packet = av_packet_alloc();
    frame = av_frame_alloc();
}

- (CVPixelBufferRef)readVideoFrame {
    while (true) {
        int ret = av_read_frame(fmt_ctx, packet);
        if (packet->stream_index == 1) {
            av_packet_unref(packet);
            continue;
        }
        if (AVERROR_EOF == ret) {
            //eof mp4
            break;
        }
        
        ret = avcodec_send_packet(avctx, packet);
        if (ret == AVERROR(EAGAIN)) {
            continue;
        }
        av_packet_unref(packet);
        
        while (1) {
            ret = avcodec_receive_frame(avctx, frame);
            if (AVERROR(EAGAIN) == ret) {
                //需要更多packet
                break;
            } else if (AVERROR_EOF == ret) {
                //
                break;
            } else {
                NSMutableData *data = [NSMutableData new];
                int w = frame->width;
                int h = frame->height;
                int yRow = frame->linesize[0];
                int uRow = frame->linesize[1];
                int vRow = frame->linesize[2];
                
                for (int i = 0; i < h; i ++) {
                    [data appendBytes:frame->data[0] + i * yRow length: w];
                }
                
                for (int i = 0; i < (h / 2); i ++) {
                    [data appendBytes:frame->data[1] + i * uRow  length: w / 2];
                }
                
                for (int i = 0; i < (h / 2); i ++) {
                    [data appendBytes:frame->data[2] + i * vRow  length: w / 2];
                }
                
                CVPixelBufferRef px = [YUVConvertor createCVPixelBufferRefFromBuffer:data type:I420 width:frame->width height:frame->height];
                if (px) {
                    return px;
                }
            }
        }
    }
    return NULL;
}

- (void)read:(void (^)(CVPixelBufferRef _Nonnull))callback {
    AVFormatContext *fmt_ctx = NULL;
    fmt_ctx = avformat_alloc_context();
    if (fmt_ctx) {
        char *file_name = "/Users/liuke/Downloads/test.mp4";
        int err = avformat_open_input(&fmt_ctx, file_name, NULL, NULL);
        avformat_find_stream_info(fmt_ctx, NULL);
        av_dump_format(fmt_ctx, 0, file_name, NULL);
        
        AVCodecContext *avctx = avcodec_alloc_context3(NULL);
        int ret = avcodec_parameters_to_context(avctx, fmt_ctx->streams[0]->codecpar);
        const AVCodec *codec = avcodec_find_decoder(avctx->codec_id);
        ret = avcodec_open2(avctx, codec, NULL);
        
        AVPacket *packet = av_packet_alloc();
        AVFrame *frame = av_frame_alloc();
        
        while (true) {
            ret = av_read_frame(fmt_ctx, packet);
            if (packet->stream_index == 1) {
                av_packet_unref(packet);
                continue;
            }
            if (AVERROR_EOF == ret) {
                //eof mp4
                break;
            }
            
            ret = avcodec_send_packet(avctx, packet);
            if (ret == AVERROR(EAGAIN)) {
                continue;
            }
            av_packet_unref(packet);
            
            while (1) {
                ret = avcodec_receive_frame(avctx, frame);
                if (AVERROR(EAGAIN) == ret) {
                    //需要更多packet
                    break;
                } else if (AVERROR_EOF == ret) {
                    //
                    break;
                } else {
                    NSMutableData *data = [NSMutableData new];
                    int w = frame->width;
                    int h = frame->height;
                    int yRow = frame->linesize[0];
                    int uRow = frame->linesize[1];
                    int vRow = frame->linesize[2];
                    
                    for (int i = 0; i < h; i ++) {
                        [data appendBytes:frame->data[0] + i * yRow length: w];
                    }
                    
                    for (int i = 0; i < (h / 2); i ++) {
                        [data appendBytes:frame->data[1] + i * uRow  length: w / 2];
                    }
                    
                    for (int i = 0; i < (h / 2); i ++) {
                        [data appendBytes:frame->data[2] + i * vRow  length: w / 2];
                    }
                    
                    CVPixelBufferRef px = [YUVConvertor createCVPixelBufferRefFromBuffer:data type:I420 width:frame->width height:frame->height];
                    if (px) {
                        callback(px);
                    }
                    [NSThread sleepForTimeInterval:0.03];
                }
            }
        }
        
        
        av_frame_free(&frame);
        av_packet_free(&packet);
        
    }
}
@end
